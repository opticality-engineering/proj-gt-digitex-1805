.PHONY: all
all: demo/index.html

.PHONY: site
site: demo/index.html
	mkdir -p static/demo
	cp demo/index.html static/demo
	cp -r demo/{css,csv,json,img,styles} static/demo

%.html: %.org
	emacsclient -e '(with-current-buffer (find-file "$<") (org-html-export-to-html))'

clean:
	rm -rf *.py *.pyc static/*
