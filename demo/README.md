# BAM Demo

Un sitio para demostrar la solución de Opticality

El "static site generator" es Emacs. El comando está en el Makefile.

## Building

```shell

  $ make
  $ make site

```

## Developing

Probar localmente:

```shell
  
  $ python -m SimpleHTTPServer 1337

```
