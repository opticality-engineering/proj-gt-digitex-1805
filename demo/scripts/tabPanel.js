jQuery(function($) {

    hideDescriptions("panel-description", 1);
    hideImages("panel-image", 1);

    $( "#panel-clasificacion" ).click(function() {
        removeAndHide();

        $('#panel-clasificacion').addClass('selection');
        $('#image-clasificacion').show();
        $('#descripcion-clasificacion').show();
    });

    $( "#panel-extraccion" ).click(function() {
        removeAndHide();

        $('#panel-extraccion').addClass('selection');
        $('#image-extraccion').show();
        $('#descripcion-extraccion').show();
    });

    $( "#panel-conexion" ).click(function() {
        removeAndHide();

        $('#panel-conexion').addClass('selection');
        $('#image-conexion').show();
        $('#descripcion-conexion').show();
    });

    function hideDescriptions(className, startIndex) {
        var descriptionTexts = document.getElementsByClassName(className);

        for(var i=startIndex; i< descriptionTexts.length; i++) {
            var elemento = "#" + descriptionTexts[i].id.toString();
            $(elemento).hide();
        }
    }

    function hideImages(className, startIndex) {
        var images = document.getElementsByClassName(className);

        for(var i=startIndex; i< images.length; i++) {
            var elemento = "#" + images[i].id.toString();
            $(elemento).hide();
        }
    }

    function removeAndHide() {
        var tab = document.getElementsByClassName("selection");

        if (tab !== undefined) {
            var elemento = "#" + tab[0].id.toString();
            $(elemento).removeClass("selection");
            hideDescriptions("panel-description", 0);
            hideImages("panel-image", 0);
        }
    }
});
